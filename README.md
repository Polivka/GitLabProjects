## GitLab Projects
Simple plugin that is adding support for GitLab specific actions to JetBrain IDEs

### Features:
* **GitLab Checkout dialog** - list all users projects on GitLab and provides quick way to clone them locally
* **GitLab Share dialog** - allows quick import of new projects to GitLab, user can specify namespace and project visibility
* **GitLab Merge Request dialog**  - user can quickly create new merge requests from current branch

### Changelist:
- **2. 11. 2015**
  * New GitLab logo
  * Add Create Merge Request Dialog
- **28. 9. 2015**
  * Added support for Import to VCS
- **10. 9. 2015**
  * Added support for GitLab Checkout Dialog

### Contributing
Everybody is welcome to contribute to this project.

Submit your pull requests to **develop** branch.

Master is always exact code that is used in latest release.